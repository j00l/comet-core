<?php

if(!defined('Comet'))die('hacking attempt.');

include_once $_SERVER['DOCUMENT_ROOT'] . '/plugins/comet_module.php';

// Western page module

class Cats extends module {
	public $name = 'Cats 1.0 alpha';

	public function __construct ($debug = 0){
		parent::__construct($debug);

		$this->proc();
	}

	private function proc (){
		$page = '<img src="plugins/Cats/images/cats_logo.gif">';

		$this->_insert($page);
	}
}

return new Cats($this->debug);