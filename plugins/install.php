<?php

$array = array(
		"Western"	=> array(
			"exec"	=> "Western/Western.php",
			"args"	=> array(
			"shortly"		=> "Shorty about Western.",
			"copyright"		=> "All j00l's rights are received."
		)),
		"Cats"		=> array(
			"exec"	=> "Cats/Cats.php",
			"args"	=> array(
				"man"			=> "God's grace to read about every project.",
				"projects"	=> "Collection of God's manuscripts."
		))
);

$enarray = json_encode($array);
file_put_contents("plugins.json", $enarray);