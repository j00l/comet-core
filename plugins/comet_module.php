<?php
if(!defined("Comet"))die("hacking attempt.");

abstract class module{
//Base class for every page modules
	public $name;
	protected $page;
	protected $debug;

	public function __construct ($debug = 0){
		$this->debug = $debug;
		if($this->debug == 1)print "<p>[".$this->name."] has started</p>";
	}

	public function _insert ($var){
		$this->page .= $var;
	}

	public function _get (){
		return $this->page;
	}
}