<?php

if(!defined('Comet'))die('hacking attempt.');

include_once $_SERVER['DOCUMENT_ROOT'] . '/plugins/comet_module.php';

// Western page module

class Western extends module {
	public $name = 'Western 1.0';

	public function __construct ($debug = 0){
		parent::__construct($debug);

		$this->proc();
	}

	private function proc (){
		$page = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/plugins/Western/template/app.tpl');

		parent::_insert($page);
	}
}


return new Western($this->debug);