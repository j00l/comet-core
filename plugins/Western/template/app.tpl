<head>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>

<div id='Western'>
	<h1>{{ message }}</h1>
	<img-item></img-item>
</div>



<script type="text/javascript">
	var westerm = new Vue({
		el: '#Western',
		data: {
			message: 'Western 1.0 - alpha of my board game for my friends.'
		}
	});

	Vue.component('img-item', {
		template: '<img src="/plugins/Western/template/images/cats_logo.gif" />'
	});

</script>