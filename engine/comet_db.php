<?php
	
class comet_c extends mysqli{

	public function __construct ($host, $user, $pass, $db){
		parent::init();

		if(!parent::options(MYSQLI_INIT_COMMAND, 'Set AUTOCOMMIT = 0'))
			die('COMET_INIT_COMMAND definition error');

		if(!parent::options(MYSQLI_OPT_CONNECT_TIMEOUT, 5))
			die('COMET_OPT_CONNECT_TIMEOUT definition error');

		if(!parent::real_connect($host, $user, $pass, $db))
			die('COMET_REAL_CONNECT error [' . mysqli_connect_errno() . '] ' . mysqli_connect_error());
	}

}