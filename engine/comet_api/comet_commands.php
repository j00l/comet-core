<?php 

class сomet_commands_c{
	
	protected $procedQuery;
	protected $unprocedQuery;

	function __construct (){}

	public function insert ($query){
		if($query['terminal'] != NULL and $query['app'] != NULL)
			$this->unprocedQuery = $query;
		else
			$this->unprocedQuery = array('terminal' => 'man', 'app' => 'Cats', 'argv' => 'full');
	}

	protected function proc (){
		$re = '/([\w\s\d]+)/m';
		foreach ($this->unprocedQuery as $key => $value) {
			preg_match_all($re, $value, $matches, PREG_SET_ORDER, 0);
			$this->procedQuery[$key] = $matches[0][0];
		}
	}

	public function get(){
		$this->proc();

		return $this->procedQuery;
	}
}