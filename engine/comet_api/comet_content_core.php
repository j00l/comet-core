<?php

// Comet content core
include_once 'comet_cc_exception.php';
class Comet_cc {

	protected $name = 'Comet content core';
	protected $app;
	protected $app_config;
	protected $app_list;
	protected $debug;

	public function __construct ($debug = 0){
		$this->debug = $debug;
		if($this->debug == 1)print "[".$this->name."] has started.<br/>";
	}

	public function insert($command){
		switch ($command['terminal']) {
			case 'man':
				$this->find($command['app']);
				break;
			
			default:
				$this->find('Cats');
				break;
		}
		
	}

	private function find ($app){

		$app_list = $this->_getlist();

		if(!empty($app_list[ucfirst($app)]))
				$this->app_config = $app_list[ucfirst($app)];

		if($this->app_config == null)
			// "There is no project with this name."
			return -1;

//==========================================
		unset($app_list);
//==========================================
	}

	public function call (){
		if($this->app_config != null)
			try{
				$this->app = include_once $_SERVER['DOCUMENT_ROOT'] . "/plugins/" . $this->app_config['exec'];
				return $this->app->_get();
			}catch(Exception $e){
				throw new comet_cc_exception('Message', $this->debug, $e, $this->debug);
			}
	}

	private function _getlist (){
		$file = utf8_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/plugins/plugins.json"));

		return json_decode($file, true);
	}

	public function get_apps_list(){
		$list = array();
		foreach(array_keys($this->_getlist()) as $val){
			array_push($list, $val);
		}
		return $list;
	}

}