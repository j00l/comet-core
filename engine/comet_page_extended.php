<?php
/**
 * COMET TPL CLASS
 */

include_once 'engine/comet_page.php';
class comet_page_extended extends comet_page{

	public function get_header ($path){

		return '<head>'.$this->set_template_part($path).'</head>';
	}

	public function get_app_window (){
		return $this->content_link->call();
	}

	public function insert_style ($styles){
		if(is_array($styles))
			foreach ($styles as $value) {
				$this->head .= '<link rel="stylesheet" type="text/css" href="'.$value.'">';
			}

		return $this->head;
	}

	public function get_apps_title ($tag = ''){
		if($tag != '')
			$tpl = '<'.$tag.'>{title}</'.$tag.'>';
		else
			$tpl = '{title}';

		$list = $this->content_link->get_apps_list();

		foreach ($list as $value) {
			$part .= str_replace('{title}', '<a href="/?terminal=man&app='.$value.'&argv=full">'.$value.'</a>', $tpl);
		}
		return $part;
	}

	public function set_title(){}
}