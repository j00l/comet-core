<?php

/**
 * COMET TPL CLASS
 */
class comet_page{

	protected $mod_version;
	protected $template_path;
	protected	$debug;
	protected $head;
	protected $page;
	protected $query;
	protected $content_link;

	function __construct ($template_name, $content_class_link, $debug = 0){
		$this->template_path = '/templates/'.$template_name.'/';
		$this->debug = $debug;
		$this->query = $query;
		$this->content_link = $content_class_link;
	}

	public function render (){
		return $this->page;
	}

	public function set_template_part ($part_path){
			$part = include_once $this->template_path.$part_path;
	}
}