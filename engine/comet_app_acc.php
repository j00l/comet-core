<?php

class app_list{
	private $app_list = null;

	function __construct (){
		$this->app_list = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/plugins/plugins.json'));
	}

	public _get (){
		return $this->app_list;
	}
}