<html>
<?php
/*
**
**
**
** It's main part of template
** Here we call every parts and do some functions
**
**
**
*/

print $this->get_header('template_parts/head/head.php');
?>
<body>
	<div class='row'>
		<div class='col-9 left'>
			<?php $this->set_template_part('template_parts/body/app.php');?>
		</div>
		<div class='col-3 right'>
			<?php $this->set_template_part('template_parts/body/apps.php');?>
		</div>
		</div>
		<div class='row'>
			<div class='col-12'>
				<?php $this->set_template_part('template_parts/body/terminal.php');?>
			</div>
		</div>
	</div>

</body>
</html>


<!--<script src="/templates/oncats/scripts/app.js" type="text/javascript"></script>
<script src="/templates/oncats/scripts/apps.js" type="text/javascript"></script>
<script src="/templates/oncats/scripts/terminal.js" type="text/javascript"></script>-->